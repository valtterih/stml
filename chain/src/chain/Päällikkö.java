package chain;

public class Päällikkö extends Pyyntö {
	private final double ALLOWABLE = 5 + BASE;

	@Override
	public void pyyntöReq(PyyntöReq request) {

		if (request.getKorotus() <= ALLOWABLE) {
			System.out.println("Päällikkö " + request.getKorotus() + " prosentin korotus hyväksytty! \n");
		} else if (successor != null) {
			System.out.println("Liian rikasta tälle päällikölle, ylemmäs menee.");
			successor.pyyntöReq(request);
		}

	}

}
