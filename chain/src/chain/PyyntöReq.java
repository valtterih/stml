package chain;

public class PyyntöReq {
	private double korotus;

	public PyyntöReq(double korotus) {
		this.korotus = korotus;
	}

	public double getKorotus() {
		return korotus;
	}

	public void setKorotus(double koro) {
		korotus = koro;
	}
}
