package chain;

public abstract class Pyyntö {

	protected static final double BASE = 0;
	protected Pyyntö successor;

	public void setSuccessor(Pyyntö successor) {
		this.successor = successor;
	}

	abstract public void pyyntöReq(PyyntöReq request);
}