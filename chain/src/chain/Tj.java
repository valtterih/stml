package chain;

public class Tj extends Pyyntö {
	private final double ALLOWABLE = 25;

	@Override
	public void pyyntöReq(PyyntöReq request) {

		if (request.getKorotus() <= ALLOWABLE) {
			System.out.println("Toimari: " + request.getKorotus() + " prosentin korotus hyväksytty! \n");
		} else {
			System.out.println("Pyysit liikaa, saat potkut! \n");
		}

	}

}
