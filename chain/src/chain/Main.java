package chain;


public class Main {
	public static void main(String[] args) {

		Esimies esimies = new Esimies();
		Päällikkö päällikkö = new Päällikkö();
		Tj tj = new Tj();
		esimies.setSuccessor(päällikkö);
		päällikkö.setSuccessor(tj);


		esimies.pyyntöReq(new PyyntöReq(2));
		esimies.pyyntöReq(new PyyntöReq(3.5));
		esimies.pyyntöReq(new PyyntöReq(25));
		esimies.pyyntöReq(new PyyntöReq(200));
	}
}
