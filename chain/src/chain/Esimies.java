package chain;

public class Esimies extends Pyyntö {
	private final double ALLOWABLE = 2 + BASE;

	@Override
	public void pyyntöReq(PyyntöReq request) {

		if (request.getKorotus() <= ALLOWABLE) {
			System.out.println("Esimies: " + request.getKorotus() + " prosentin korotus hyväksytty! \n");
		} else if (successor != null) {
			System.out.println("Liian rikasta tälle esimiehelle, ylemmäs menee.");
			successor.pyyntöReq(request);
		}

	}
}
