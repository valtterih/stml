package flyweight;

public class StringTest {
	public static void main(String[] args) {
		String fly = "fly", weight = "weight";
		String fly2 = "fly", weight2 = "weight";
		System.out.println(fly == fly2); // fly and fly2 refer to the same String instance
		System.out.println(weight == weight2); // weight and weight2 also refer to
												// the same String instance
		String distinctString = fly + weight;
		System.out.println(distinctString == "flyweight"); // flyweight and "flyweight" do not
															// refer to the same instance
		String flyweight = (fly + weight).intern();
		System.out.println(flyweight == "flyweight"); // The intern() method returns a flyweight
		
		String s1=new String("hello");  
		String s2="hello";  
		String s3=s1.intern();//returns string from pool, now it will be same as s2  
		System.out.println(s1==s2);//false because reference variables are pointing to different instance  
		System.out.println(s2==s3);//true because reference variables are pointing to same instance  
	}
}
