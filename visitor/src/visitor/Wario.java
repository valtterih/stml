package visitor;

public class Wario implements State {

	private int bonusPoints;

	public int getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	private Wario() {
	}

	private static final Wario instance = new Wario();

	public static State getInstance() {
		return instance;
	}

	public void vaihto(final Hahmo Hahmo) {
		Hahmo.setState(Luigi.getInstance());
	}

	public void printStage() {
		System.out.println("waaaaaaaaaa: " + bonusPoints);
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
