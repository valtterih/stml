package visitor;

public class Main {
	public static void main(String[] args) {
		
		final Hahmo hahmo = new Hahmo();
		Visitor visitor = new HahmoVisitor();

		hahmo.accept(visitor);
		hahmo.printStage();
		hahmo.vaihto();
		hahmo.accept(visitor);
		hahmo.printStage();
		hahmo.vaihto();
		hahmo.accept(visitor);
		hahmo.printStage();
		hahmo.vaihto();


	}
}
