package visitor;

public class HahmoVisitor implements Visitor {

	public void visit(Wario state) {
		state.setBonusPoints(1);
	}

	public void visit(Luigi state) {
		state.setBonusPoints(2);
	}

	public void visit(Mario state) {
		state.setBonusPoints(3);
	}
}
