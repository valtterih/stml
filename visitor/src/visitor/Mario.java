package visitor;

public class Mario implements State {

	private int bonusPoints;

	public int getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	private Mario() {
	}

	private static final Mario instance = new Mario();

	public static State getInstance() {
		return instance;
	}

	public void vaihto(final Hahmo Hahmo) {
	}

	public void printStage() {
		System.out.println("mariolla on näin paljon pisteitä: " + bonusPoints);
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
