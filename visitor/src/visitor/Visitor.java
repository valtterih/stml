package visitor;

public interface Visitor {
	
	void visit(Mario state);

	void visit(Luigi state);

	void visit(Wario state);
}
