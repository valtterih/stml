package visitor;

public class Hahmo {

	private State state;

	public Hahmo() {
		setState(Wario.getInstance());
	}

	void setState(final State newState) {
		state = newState;
	}

	public void vaihto() {
		state.vaihto(this);
	}

//	public void hyppy() {
//		state.hyppy(this);
//	}

	public void printStage() {
		state.printStage();
	}

	public void accept(Visitor visitor) {
		state.accept(visitor);
	}
}
