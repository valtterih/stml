package visitor;

public class Luigi implements State {

	private int bonusPoints;

	public int getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	private Luigi() {
	}

	private static final Luigi instance = new Luigi();

	public static State getInstance() {
		return instance;
	}

	public void vaihto(final Hahmo Hahmo) {
		Hahmo.setState(Mario.getInstance());
	}

	public void printStage() {
		System.out.println("luigi pisteet" + bonusPoints);
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
