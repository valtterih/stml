package visitor;

public interface State {
	
	public void setBonusPoints(int bonusPoints);

	public int getBonusPoints();

	public void vaihto(final Hahmo hahmo);

	public void printStage();

	public void accept(Visitor visitor);
}
