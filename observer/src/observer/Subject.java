package observer;

import java.util.*;

public class Subject {
	
	Set<Observer> observers = new HashSet();
	
	public void attach(Observer o) {
		observers.add(o);
	}
	
	public void detach(Observer o) {
		observers.remove(o);
	}
	
	protected void Notify() {
		for (Observer o: observers) {
			o.update(this);
		}
	}

}