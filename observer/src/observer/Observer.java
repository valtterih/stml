package observer;

import java.util.*; //tai observable + observer

public interface Observer {
	
	public abstract void update(Subject o);
	

}