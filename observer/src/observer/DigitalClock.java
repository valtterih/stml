package observer;

import java.util.*;

public class DigitalClock implements Observer {
	
	private ClockTimer timer;

	public DigitalClock(ClockTimer ct) {
		timer = ct;
		timer.attach(this);
	}

	@Override
	public void update(Subject o) {
		if (o == timer) {
			draw();
		}

	}

	private void draw() {
		int hour = timer.getHour();
		int minute = timer.getMinute();
		int second = timer.getSecond();
		//ei 00:00:00 vaan 0:0:0
		System.out.println("klo " + hour + ":" + minute + ":" + second);
	}

}