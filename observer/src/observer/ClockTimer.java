package observer;

import java.util.*;

public class ClockTimer extends Subject {
	
	//muuttujat
	public int hour = 0;
	public int minute = 0;
	public int second = 0;
	
	
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public int getSecond() {
		return second;
	};
	
	void tick() {
		second = second + 1;
		//s to m
		if (second == 60) { 
			minute = minute + 1;
			second = 0;
			//m to h
			if (minute == 60) {
				hour = hour + 1;
				minute = 0;
				//h to 0 or d
				if (hour == 24) {
					hour = 0;
				}
			}
		}
		Notify();
	}

}