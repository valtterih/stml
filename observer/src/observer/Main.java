package observer;

import java.util.*;

public class Main {
	public static void main(String[] args) {
		
		
		ClockTimer timer = new ClockTimer();
		//AnalogClock analogClock = new AnalogClock(timer);
		DigitalClock digitalClock = new DigitalClock(timer);
		

		digitalClock.update(timer);
		timer.tick();
		timer.tick();
		timer.tick();
		
	}

}