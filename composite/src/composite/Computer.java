package composite;

import java.util.*;

public class Computer implements Part {

	int hinta = 0;
	String kuvaus = "Kokonainen tietokone";
	List<Part> osat = new ArrayList<Part>();

	@Override
	public int hinta() {
		for (Part part : osat) {
			hinta = hinta + part.hinta();
		}
		return hinta;

	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}

	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: " + hinta());
	}

	@Override
	public void addPart(Part part) {
		osat.add(part);
	}

	public void osat() {
		for (Part part : osat) {
			part.tuloste();
		}

	}

}
