package composite;

public class Ssd implements Part {

	int hinta = 80;
	String kuvaus = "ssd";
	
	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}
	
	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: "+ hinta);
	}
	
	@Override
	public void addPart(Part part) {
	}
}
