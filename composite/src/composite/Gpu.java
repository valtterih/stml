package composite;

public class Gpu implements Part {

	int hinta = 300;
	String kuvaus = "näyttis";
	
	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}
	
	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: "+ hinta);
	}
	
	@Override
	public void addPart(Part part) {
	}
}
