package composite;

public class Cpu implements Part {

	int hinta = 250;
	String kuvaus = "prossu";
	
	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}
	
	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: "+ hinta);
	}
	
	@Override
	public void addPart(Part part) {
	}
}
