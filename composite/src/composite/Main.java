package composite;

public class Main {
	
	public static void main(String[] args) {

		Part Case = new Case();
		Part Cpu = new Cpu();
		Part Fan = new Fan();
		Part Gpu = new Gpu();
		Part Mobo = new Mobo();
		Part Ram = new Ram();
		Part Ssd = new Ssd();
		Part Hdd = new Hdd();
		
		Computer PC = new Computer();
		PC.addPart(Case);
		PC.addPart(Cpu);
		PC.addPart(Fan);
		PC.addPart(Gpu);
		PC.addPart(Mobo);
		PC.addPart(Ram);
		PC.addPart(Ram);
		PC.addPart(Ssd);
		PC.addPart(Hdd);
		PC.addPart(Fan);
		PC.addPart(Fan);
		PC.osat();
		PC.tuloste();

	}

}
