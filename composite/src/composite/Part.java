package composite;

public interface Part {

	public int hinta();
	public String kuvaus();
	public void tuloste();
	public void addPart(Part part);
	
}
