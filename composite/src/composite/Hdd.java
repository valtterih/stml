package composite;

public class Hdd implements Part {

	int hinta = 50;
	String kuvaus = "kovo";
	
	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}
	
	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: "+ hinta);
	}
	
	@Override
	public void addPart(Part part) {
	}
}
