package composite;

public class Case implements Part {

	int hinta = 100;
	String kuvaus = "kotelo";

	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}

	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: " + hinta);
	}

	@Override
	public void addPart(Part part) {
	}

}
