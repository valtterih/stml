package composite;

public class Fan implements Part {

	int hinta = 10;
	String kuvaus = "tuuletin";
	
	@Override
	public int hinta() {
		return hinta;
	}

	@Override
	public String kuvaus() {
		return kuvaus;
	}
	
	@Override
	public void tuloste() {
		System.out.println(kuvaus + ", hinta: "+ hinta);
	}
	
	@Override
	public void addPart(Part part) {
	}
}
