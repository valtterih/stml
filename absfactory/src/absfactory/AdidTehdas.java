package absfactory;

public class AdidTehdas implements VaateTehdas {
	public AdidTehdas() {
		
	}
	public Paita createPaita() {
		return new AdidPaita();
	}
	public Farkut createFarkut() {
		return new AdidFarkut();
	}
	public Hattu createHattu() {
		return new AdidHattu();
	}
	public Kengät createKengät() {
		return new AdidKengät();
	}

}
