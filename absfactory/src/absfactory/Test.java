package absfactory;

import java.io.*;
import java.util.*;

public class Test {
	
	public static void main(String[] args) {
		
		/* originaali absfactory
		Jasper jasper = new Jasper();
		
		jasper.puvusto(new AdidTehdas());
		jasper.luetteleSetti();
		
		jasper.puvusto(new BossTehdas());
		jasper.luetteleSetti();
		*/
		
		Jasper jasper = new Jasper();

		Class c = null;
		VaateTehdas tehdas = null;
		
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("src/absfactory/tehdas.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
		    c = Class.forName(properties.getProperty("tehdas"));
			tehdas = (VaateTehdas)c.newInstance();
		} catch (Exception e) {
			e.printStackTrace();			}
		
		//System.out.println(tehdas); //käsinvarmistus
		
		jasper.puvusto(tehdas);
		jasper.luetteleSetti();
	}
		
}
