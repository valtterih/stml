package absfactory;

public class BossTehdas implements VaateTehdas {
public BossTehdas() {
		
	}
	public Paita createPaita() {
		return new BossPaita();
	}
	public Farkut createFarkut() {
		return new BossFarkut();
	}
	public Hattu createHattu() {
		return new BossHattu();
	}
	public Kengät createKengät() {
		return new BossKengät();
	}


}
