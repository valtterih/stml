package state;

public class HyperMario extends PlumberState {
	
	private static HyperMario state = null;
	
	public static HyperMario getState() {
		if (state == null) {
		state = new HyperMario();
		}
		return state;
	}
	
	public void liiku(Plumber tyyppi) {
		System.out.println("*Juoksua*");
	}
	
	public void puhu(Plumber tyyppi) {
		System.out.println("MAAAAARIO");
	}
	
	public void hyppää(Plumber tyyppi) {
		System.out.println("*Kolmoishyppy*");
	}
	public void iso(Plumber tyyppi) {
		kasva(tyyppi, SuperMario.getState());
	}

}
