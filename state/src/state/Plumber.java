package state;

public class Plumber {
	
	private PlumberState tyyppi;
	
	public Plumber() {
		tyyppi = Mario.getState();
	}
	
	protected void kasva(PlumberState kasvu) {
		tyyppi = kasvu;
	}
	
	public void iso() {
		tyyppi.iso(this);
	}
	public void liiku() {
		tyyppi.liiku(this);
	}
	public void puhu() {
		tyyppi.puhu(this);
	}
	public void hyppää() {
		tyyppi.hyppää(this);
	}

}
