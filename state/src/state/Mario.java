package state;

public class Mario extends PlumberState {
	
	private static Mario state = null;
	
	public static Mario getState() {
		if (state == null) {
		state = new Mario();
		}
		return state;
	}
	
	public void liiku(Plumber tyyppi) {
		System.out.println("*Kävelyä*");
	}
	
	public void puhu(Plumber tyyppi) {
		System.out.println("ITS");
	}
	
	public void hyppää(Plumber tyyppi) {
		System.out.println("*Hyppy*");
	}
	public void iso(Plumber tyyppi) {
		kasva(tyyppi, SuperMario.getState());
	}

}
