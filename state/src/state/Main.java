package state;

public class Main {
	
	public static void main(String[] args) {

		Plumber mario = new Plumber();
		
		mario.liiku();
		mario.puhu();
		mario.hyppää();
		mario.iso();
		mario.liiku();
		mario.puhu();
		mario.hyppää();
		mario.iso();
		mario.liiku();
		mario.puhu();
		mario.hyppää();
	}
	

}
