package state;

public class SuperMario extends PlumberState {
	
	private static SuperMario state = null;
	
	public static SuperMario getState() {
		if (state == null) {
		state = new SuperMario();
		}
		return state;
	}
	
	public void liiku(Plumber tyyppi) {
		System.out.println("*Hölkkää*");
	}
	
	public void puhu(Plumber tyyppi) {
		System.out.println("AME");
	}
	
	public void hyppää(Plumber tyyppi) {
		System.out.println("*Kaksoishyppy*");
	}
	public void iso(Plumber tyyppi) {
		kasva(tyyppi, HyperMario.getState());
	}

}
