package strategy;

import java.util.*;

public class Main {
	
	public static void main(String[] args) {
		
		EkaStrat strat1 = new EkaStrat();
        TokaStrat strat2 = new TokaStrat();
        KolStrat strat3 = new KolStrat();

        List<String> list = Arrays.asList("yy", "2222", "koo", "44444", "vii", "66666", "seit");


        System.out.println("Jokainen:");
        System.out.println(strat1.listToString(list));
        System.out.println("\nJoka toinen:");
        System.out.println(strat2.listToString(list));
        System.out.println("\njoka kolmas:");
        System.out.println(strat3.listToString(list));
        System.out.println("\n");

	}

}
