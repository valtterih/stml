package strategy;

import java.util.*;

public class TokaStrat implements ListaString {
	
	@Override
    public String listToString(List list) {
		
        StringBuilder string = new StringBuilder();
        for(int i = 0; i < list.size(); i++) {
        	
        	
        	//!!!!!!!!!!!!!+1 tai toka jo seuraavalla rivillä
            if((i+1) % 2 == 0) {
                string.append(list.get(i)).append("\n");
            } else {
                string.append(list.get(i));
            }
        }
        
        return string.toString();
	}


}
