package strategy;

import java.util.*;

public class EkaStrat implements ListaString {

	@Override
    public String listToString(List list) {
		
        StringBuilder string = new StringBuilder();
        
        for(Object o : list) {
            string.append(o).append("\n");
        }
        
        return string.toString();
    }


}
