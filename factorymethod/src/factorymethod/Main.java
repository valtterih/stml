package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus opiskelija = new Opiskelija();
        AterioivaOtus vahtimestari = new Vahtimestari();
        opettaja.aterioi();
        opiskelija.aterioi();
        vahtimestari.aterioi();
    }
}
