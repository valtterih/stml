package decorator;

public class Ananas extends Täyte {
	
	private double omaHinta = 1.2;
	private String description = "Makea ananas";

	public Ananas(Pizza pohja) {
		super(pohja);
	}

	public double getHinta() {
		return super.getHinta() + omaHinta;
	}

	public String getKuvaus() {
		return super.getKuvaus() + description + "\n";
	}
}
