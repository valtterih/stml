package decorator;

public class Kinkku extends Täyte {
	
	private double omaHinta = 1.5;
	private String description = "Sopivan suolainen kinkku";

	public Kinkku(Pizza pohja) {
		super(pohja);
	}

	public double getHinta() {
		return super.getHinta() + omaHinta;
	}

	public String getKuvaus() {
		return super.getKuvaus() + description + "\n";
	}

}
