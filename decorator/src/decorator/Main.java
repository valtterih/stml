package decorator;

public class Main {
	public static void main(String[] args) {
		
		System.out.println("Tänään tarjouksessa:\n");

		Pizza hawaii = new Kinkku( new Juusto( new Ananas( new Pohja())));
		System.out.println("Hawaii: " + hawaii.getHinta() + "€");
		System.out.println(hawaii.getKuvaus() + "\n");
		
		Pizza quatrofr = new Juusto( new Juusto( new Juusto( new Juusto( new Pohja()))));
		System.out.println("Quatro fromaggio: " + quatrofr.getHinta() + "€");
		System.out.println(quatrofr.getKuvaus() + "\n");

		Pizza torstai = new Hernekeitto( new Kinkku( new Pohja()));
		System.out.println("Torstai special: " + torstai.getHinta() + "€");
		System.out.println(torstai.getKuvaus() + "\n");

		Pizza plain =  new Pohja();
		System.out.println("Ilman mitään!: " + plain.getHinta() + "€");
		System.out.println(plain.getKuvaus() + "\n");


	}

}
