package decorator;

public class Pohja implements Pizza {

	private double hinta = 3.8;
	private String kuvaus = "Tuore, reunoiltaan rapea pohja, tomaattikastikkeella";

	public double getHinta() {
		return hinta;
	}

	public String getKuvaus() {
		return kuvaus + "\n";
	}

}
