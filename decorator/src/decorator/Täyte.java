package decorator;

public class Täyte implements Pizza {

	protected Pizza pohja;

	public Täyte(Pizza pohja) {
		this.pohja = pohja;
	}

	public double getHinta() {
		return pohja.getHinta();
	}

	public String getKuvaus() {
		return pohja.getKuvaus();
	}

}
