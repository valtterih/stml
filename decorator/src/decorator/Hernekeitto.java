package decorator;

public class Hernekeitto extends Täyte {
	
	private double omaHinta = 20;
	private String description = "Laskiaisherkku hernekeitto";

	public Hernekeitto(Pizza pohja) {
		super(pohja);
	}

	public double getHinta() {
		return super.getHinta() + omaHinta;
	}

	public String getKuvaus() {
		return super.getKuvaus() + description + "\n";
	}
}
