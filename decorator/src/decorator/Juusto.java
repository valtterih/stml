package decorator;

public class Juusto extends Täyte {
	
	private double omaHinta = 0.8;
	private String description = "Täyteläinen juusto\t";

	public Juusto(Pizza pohja) {
		super(pohja);
	}

	public double getHinta() {
		return super.getHinta() + omaHinta;
	}

	public String getKuvaus() {
		return super.getKuvaus() + description + "\n";
	}
}
