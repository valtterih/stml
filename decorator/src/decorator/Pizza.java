package decorator;

public interface Pizza {
	
	public double getHinta();
	public String getKuvaus();

}
