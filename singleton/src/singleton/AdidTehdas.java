package singleton;

public class AdidTehdas implements VaateTehdas {
	private static AdidTehdas INSTANCE = null;

	private AdidTehdas() {
	}

	public static AdidTehdas getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AdidTehdas();
			return INSTANCE;
		}
		else {
			throw new IllegalStateException("Käytössä jo!");
		}
	}

	public Paita createPaita() {
		return new AdidPaita();
	}

	public Farkut createFarkut() {
		return new AdidFarkut();
	}

	public Hattu createHattu() {
		return new AdidHattu();
	}

	public Kengät createKengät() {
		return new AdidKengät();
	}

}
