package singleton;

public interface VaateTehdas {
	public abstract Farkut createFarkut();
	public abstract Paita createPaita();
	public abstract Hattu createHattu();
	public abstract Kengät createKengät();
}
