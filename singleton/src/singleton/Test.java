package singleton;

public class Test {
	
	public static void main(String[] args) {
		Jasper jasper = new Jasper();
		PahaJasper pahajasper = new PahaJasper();
		
		jasper.puvusto(AdidTehdas.getInstance());
		jasper.luetteleSetti();
		
		jasper.puvusto(BossTehdas.getInstance());
		jasper.luetteleSetti();
		
		pahajasper.puvusto(BossTehdas.getInstance());
		pahajasper.luetteleSetti();

		
	}
}
