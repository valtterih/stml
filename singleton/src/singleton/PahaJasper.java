package singleton;

public class PahaJasper {
	private Farkut farkut = null;
	private Paita paita = null;
	private Hattu hattu = null;
	private Kengät kengät = null;
	
	public void puvusto(VaateTehdas tehdas) {
		farkut = tehdas.createFarkut();
		paita = tehdas.createPaita();
		hattu = tehdas.createHattu();
		kengät = tehdas.createKengät();
	}
	
	public void luetteleSetti() {
		System.out.println("Päälläni on: " + "\n" + farkut + "\n" + paita + "\n" + hattu +"\n" + kengät + "\n" + "erittäin tyylikästä eikö?" + "\n");
	}

}
