package singleton;

public class BossTehdas implements VaateTehdas {
	
	private static BossTehdas INSTANCE = null;

	private BossTehdas() {
	}
	
	public static BossTehdas getInstance() {
		if(INSTANCE == null){
			INSTANCE = new BossTehdas();
			return INSTANCE;
		}
		else {
			throw new IllegalStateException("Käytössä jo!");
		}
	}
	

	public Paita createPaita() {
		return new BossPaita();
	}

	public Farkut createFarkut() {
		return new BossFarkut();
	}

	public Hattu createHattu() {
		return new BossHattu();
	}

	public Kengät createKengät() {
		return new BossKengät();
	}

}
